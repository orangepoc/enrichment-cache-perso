package io.gitlab.zc.enrichmentcache.infrastructure.adapters.outbound.redis;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CacheStoreRedisAdapterTest {

    @Test
    void normKey() {
        // GIVEN
        String key = "test";

        // WHEN
        var normKey = CacheStoreRedisAdapter.normKey(key);

        // THEN
        assertThat(normKey).isEqualTo("intervention:cache:test");
    }
}
