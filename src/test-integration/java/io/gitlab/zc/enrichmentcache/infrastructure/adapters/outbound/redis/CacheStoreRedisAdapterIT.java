package io.gitlab.zc.enrichmentcache.infrastructure.adapters.outbound.redis;

import static org.assertj.core.api.Assertions.*;

import java.time.LocalDateTime;

import org.assertj.core.api.InstanceOfAssertFactories;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.utility.DockerImageName;

import com.redis.testcontainers.RedisContainer;

import io.gitlab.zc.enrichmentcache.core.domain.FinExternalData;
import io.gitlab.zc.enrichmentcache.core.ports.spi.CacheStore;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
class CacheStoreRedisAdapterIT {

    private static final int REDIS_PORT_NUM = 6379;

    @Container
    private static final RedisContainer REDIS_CONTAINER =
            new RedisContainer(DockerImageName.parse("redis:7-alpine"))
                    .withExposedPorts(REDIS_PORT_NUM);

    @Autowired
    CacheStore underTest;

    // test fixtures
    String key = "test";
    LocalDateTime dateTime = LocalDateTime.of(2023, 4, 28, 14, 45, 58, 790713400);
    FinExternalData finExternalData = new FinExternalData("status_OK", "comInt", dateTime, true);

    @DynamicPropertySource
    private static void registerRedisProperties(DynamicPropertyRegistry registry) {
        REDIS_CONTAINER.start();
        registry.add("spring.redis.host", REDIS_CONTAINER::getHost);
        registry.add("spring.redis.port", () -> REDIS_CONTAINER.getMappedPort(REDIS_PORT_NUM).toString());
    }

    @Test
    void checkRedisIsRunning() {
        assertThat(REDIS_CONTAINER.isRunning()).isTrue();
    }

    @Test
    void should_returnStoredData_when_retrievingExistingData() {
        // WHEN
        underTest.store(key, finExternalData);
        var retrieved = underTest.retrieve(key);

        // THEN
        assertThat(retrieved).get(as(InstanceOfAssertFactories.type(FinExternalData.class))).isEqualTo(finExternalData);
    }

    @Test
    void should_returnOptionalEmpty_when_retrievingNonExistingData() {
        // WHEN
        var retrieved = underTest.retrieve("absurd-key");

        // THEN
        assertThat(retrieved).isEmpty();
    }
}
