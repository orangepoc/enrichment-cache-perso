package io.gitlab.zc.enrichmentcache.infrastructure.adapters.outbound.redis;

import static org.assertj.core.api.Assertions.*;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Objects;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.gitlab.zc.enrichmentcache.core.domain.FinExternalData;

@SpringBootTest
class FinDataSerializerIT {

    @Autowired
    ObjectMapper mapper;

    // test data
    LocalDateTime dateTime = LocalDateTime.of(2023, 4, 28, 14, 45, 58, 790713400);
    FinExternalData finExternalData = new FinExternalData("status_OK", "comInt", dateTime, true);

    @Test
    void serialize() {
        // GIVEN
        FinDataSerializer serializer = new FinDataSerializer(Objects.requireNonNull(mapper));

        // WHEN
        var bytes = serializer.serialize(finExternalData);

        // THEN
        assertThat(bytes).asString().isEqualTo(
                "{\"status\":\"status_OK\",\"comInt\":\"comInt\",\"dateCi\":\"2023-04-28T14:45:58.7907134\",\"otConforme\":true}");
    }

    @Test
    void deserialize() {
        // GIVEN
        FinDataSerializer serializer = new FinDataSerializer(Objects.requireNonNull(mapper));

        // WHEN
        var object = serializer.deserialize(("{\"status\":\"status_OK\",\"comInt\":\"comInt\",\"dateCi\":\"2023-04-28T14:45:58"
                + ".7907134\",\"otConforme\":true}").getBytes(StandardCharsets.UTF_8));

        // THEN
        assertThat(object).isEqualTo(finExternalData);
    }
}
