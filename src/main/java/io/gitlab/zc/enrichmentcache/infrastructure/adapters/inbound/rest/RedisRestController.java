package io.gitlab.zc.enrichmentcache.infrastructure.adapters.inbound.rest;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;

import io.gitlab.zc.enrichmentcache.core.domain.FinExternalData;
import io.gitlab.zc.enrichmentcache.core.exceptions.TechnicalException;
import io.gitlab.zc.enrichmentcache.core.ports.api.ExternalDataProducer;
import io.gitlab.zc.enrichmentcache.core.ports.spi.CacheStore;

@RestController
@RequestMapping("/redis")
public class RedisRestController {

    private final JsonMapper jsonMapper;
    private final CacheStore cacheStore;
    private final ExternalDataProducer externalDataProducer;

    public RedisRestController(JsonMapper jsonMapper, CacheStore cacheStore, ExternalDataProducer externalDataProducer) {
        this.jsonMapper = jsonMapper;
        this.cacheStore = cacheStore;
        this.externalDataProducer = externalDataProducer;
    }

    @PutMapping(value = "/{name}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void store(@PathVariable String name) {
        cacheStore.store(name, externalDataProducer.produceExternalData(name));
    }

    @GetMapping(value = "/{name}", produces = "application/json")
    public String retrieve(@PathVariable String name) {
        var finExternalData = cacheStore.retrieve(name)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "no FinExternalData found for key '%s'".formatted(name)));
        return toJson(finExternalData);
    }

    private String toJson(FinExternalData finExternalData) {
        try {
            return jsonMapper.writeValueAsString(finExternalData);
        } catch (JsonProcessingException e) {
            throw new TechnicalException("fail to convert FinExternalData object to JSON", e)
                    .addContextValue("finExternalData", finExternalData);
        }
    }
}
