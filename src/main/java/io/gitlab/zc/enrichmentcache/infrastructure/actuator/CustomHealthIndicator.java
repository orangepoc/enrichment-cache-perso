package io.gitlab.zc.enrichmentcache.infrastructure.actuator;

import java.io.File;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Demonstrate adding a new health indicator into the 'health' endpoint.
 * <p>
 * It is just a POC. Remove if not used.
 */
@Component
public class CustomHealthIndicator extends AbstractHealthIndicator {

    private final Environment environment;
    private final File dir;

    protected CustomHealthIndicator(Environment environment, @Value("${application.inbox.path}") File dir) {
        super("Custom health check failed");
        this.environment = environment;
        this.dir = dir;
    }

    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        builder.withDetail("path", dir.getCanonicalPath());
        builder.withDetail("exists", dir.exists());

        if (isRunningLocally()) {
            builder.status(dir.exists() ? Status.UP : Status.DOWN);
        } else {
            // When not running locally, always return UP because we don't want
            // to fail the application startup sequence on CloudFoundry.
            // It is just a POC after-all.
            builder.status(Status.UP);
        }
    }

    protected boolean isRunningLocally() {
        var activeProfiles = environment.getActiveProfiles();
        Arrays.sort(activeProfiles);
        return Arrays.binarySearch(activeProfiles, "local") >= 0;
    }
}

/* EOF */
