package io.gitlab.zc.enrichmentcache.infrastructure.adapters.inbound.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerErrorException;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;

import io.gitlab.zc.enrichmentcache.core.exceptions.TechnicalException;

@RestController
@RequestMapping("/spring")
public class ProblemRestController {

    @GetMapping(value = "p0")
    public Object p0() {
        throw new TechnicalException("failing intentionally");
    }

    @GetMapping(value = "p1")
    public Object p1() {
        var exception = new TechnicalException("failing intentionally");
        return new ServerErrorException("p1 failed", exception);
    }

    @GetMapping(value = "p2")
    public Object p2() {
        var exception = new TechnicalException("failing intentionally");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception);
    }

    /**
     * Demonstrate using the RFC 7807 Problem DTO.
     */
    @GetMapping(value = "p3")
    public Object p3() {
        var exception = new TechnicalException("failing intentionally");
        var problemDetail = Problem.valueOf(Status.INTERNAL_SERVER_ERROR, exception.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(problemDetail);
    }
}
