package io.gitlab.zc.enrichmentcache.infrastructure.adapters.outbound.redis;

import java.io.IOException;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.gitlab.zc.enrichmentcache.core.domain.FinExternalData;
import io.gitlab.zc.enrichmentcache.core.exceptions.TechnicalException;

/**
 * A Redis Serializer that use a JSON ObjectMapper to serialize FinExternalData into a JSON String and reverse.
 */
public class FinDataSerializer implements RedisSerializer<FinExternalData> {

    private final ObjectMapper mapper;

    public FinDataSerializer(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public byte[] serialize(FinExternalData finExternalData) throws SerializationException {
        if (finExternalData == null) {
            return null;
        }

        try {
            return mapper.writerFor(FinExternalData.class).writeValueAsBytes(finExternalData);
        } catch (JsonProcessingException e) {
            throw new TechnicalException("unable to write FinExternalData to json", e);
        }
    }

    @Override
    public FinExternalData deserialize(byte[] bytes) throws SerializationException {
        if (ArrayUtils.isEmpty(bytes)) {
            return null;
        }

        try {
            return mapper.readValue(bytes, FinExternalData.class);
        } catch (IOException e) {
            throw new TechnicalException("unable to read FinExternalData from json", e);
        }
    }
}
