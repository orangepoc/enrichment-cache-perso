package io.gitlab.zc.enrichmentcache.infrastructure.actuator;

import java.util.Map;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.stereotype.Component;

import io.gitlab.zc.enrichmentcache.infrastructure.config.ApplicationConfigMap;

/**
 * Demonstrate adding a new endpoint to the actuator list of endpoint.
 */
@Component
@Endpoint(id = "featureflags")
public class FeatureFlagsEndpoint {

    private final ApplicationConfigMap applicationConfigMap;


    public FeatureFlagsEndpoint(ApplicationConfigMap applicationConfigMap) {
        this.applicationConfigMap = applicationConfigMap;
    }

    @ReadOperation
    public Map<String, Boolean> listFeatureFlags() {
        return applicationConfigMap.getFeatureFlags();
    }

    @ReadOperation
    public boolean getFeatureFlag(@Selector String name) {
        return applicationConfigMap.getFeatureFlags().get(name);
    }

    @WriteOperation
    public void setFeatureFlag(@Selector String name, boolean value) {
        applicationConfigMap.getFeatureFlags().put(name, value);
    }
}
