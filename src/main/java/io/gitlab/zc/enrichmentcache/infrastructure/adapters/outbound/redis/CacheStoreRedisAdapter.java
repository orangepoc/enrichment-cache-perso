package io.gitlab.zc.enrichmentcache.infrastructure.adapters.outbound.redis;

import java.time.Duration;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.gitlab.zc.enrichmentcache.core.domain.FinExternalData;
import io.gitlab.zc.enrichmentcache.core.ports.spi.CacheStore;

@Component
public class CacheStoreRedisAdapter implements CacheStore {

    private static final String KEY_PREFIX = "intervention:cache:";

    @Value("${application.tuple-expiration-delay.before-consumed}")
    private Duration BEFORE_CONSUMED_EXPIRATION_DELAY;

    @Value("${application.tuple-expiration-delay.after-consumed}")
    private Duration AFTER_CONSUMED_EXPIRATION_DELAY;

    private final RedisTemplate<String, FinExternalData> redisTemplate;

    public CacheStoreRedisAdapter(RedisConnectionFactory redisConnectionFactory, ObjectMapper mapper) {
        var finDataSerializer = new FinDataSerializer(mapper);

        // create the redisTemplate configured for this CacheStore
        redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        redisTemplate.setKeySerializer(RedisSerializer.string());
        redisTemplate.setValueSerializer(finDataSerializer);
        redisTemplate.setHashKeySerializer(RedisSerializer.string());
        redisTemplate.setHashValueSerializer(finDataSerializer);
        redisTemplate.afterPropertiesSet();
    }

    @Override
    public void store(String key, FinExternalData data) {
        var normKey = normKey(key);
        redisTemplate.opsForValue().set(normKey, data);
        redisTemplate.expire(normKey, BEFORE_CONSUMED_EXPIRATION_DELAY);
    }

    @Override
    public Optional<FinExternalData> retrieve(String key) {
        return Optional.ofNullable(redisTemplate.opsForValue().getAndExpire(normKey(key), AFTER_CONSUMED_EXPIRATION_DELAY));
    }

    public static String normKey(String s) {
        return s.startsWith(KEY_PREFIX) ? s : KEY_PREFIX + s;
    }
}
