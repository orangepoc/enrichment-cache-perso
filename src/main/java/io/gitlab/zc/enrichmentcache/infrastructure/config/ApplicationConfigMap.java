package io.gitlab.zc.enrichmentcache.infrastructure.config;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

/**
 * Application specific configurations.
 */
@Getter
@Setter
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "application")
public class ApplicationConfigMap {
    /**
     * Application's feature flags. Can be set using the: {@code /actuator/feature-flags/<feature-name>} endpoint.
     */
    private final Map<String, Boolean> featureFlags = new ConcurrentSkipListMap<>();

    private ExpirationDelay tupleExpirationDelay;

    /**
     * Configuration for the inbox.
     */
    private InboxConfig inbox;

    @Getter
    @Setter
    @Configuration
    public static class ExpirationDelay {
        public Duration beforeConsumed;
        public Duration afterConsumed;
    }

    @Getter
    @Setter
    @Configuration
    public static class InboxConfig {
        /**
         * The path to the directory that will be scanned periodically.
         */
        private String path;

        /**
         * the period between two scans in milliseconds.
         */
        private long scanPeriod;
    }
}

/* EOF */
