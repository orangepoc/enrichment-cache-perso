package io.gitlab.zc.enrichmentcache.infrastructure.actuator;

import java.util.Map;

import org.springframework.boot.actuate.info.Info.Builder;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import io.gitlab.zc.enrichmentcache.infrastructure.config.ApplicationConfigMap;

/**
 * Demonstrate contributing custom property groups into the 'info' endpoint response.
 */
@Component
public class CustomInfoContributor implements InfoContributor {

    private final ApplicationConfigMap applicationConfigMap;
    private final Environment environment;

    public CustomInfoContributor(ApplicationConfigMap applicationConfigMap, Environment environment) {
        this.applicationConfigMap = applicationConfigMap;
        this.environment = environment;
    }

    @Override
    public void contribute(Builder builder) {
        builder.withDetail("active-profiles", contributeActiveProfilesName());
        builder.withDetail("feature-flags", contributeFeatureFlags());
    }

    private Object contributeActiveProfilesName() {
        var activeProfiles = environment.getActiveProfiles();
        return activeProfiles.length != 0 ? activeProfiles : environment.getDefaultProfiles();
    }

    private Map<String, Boolean> contributeFeatureFlags() {
        return applicationConfigMap.getFeatureFlags();
    }
}
