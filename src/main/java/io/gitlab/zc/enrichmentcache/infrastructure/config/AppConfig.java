package io.gitlab.zc.enrichmentcache.infrastructure.config;

import java.text.SimpleDateFormat;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

import com.fasterxml.jackson.databind.json.JsonMapper;

import io.gitlab.zc.enrichmentcache.core.ports.api.ExternalDataProducer;
import io.gitlab.zc.enrichmentcache.core.services.ExternalDataProducerImpl;
import io.gitlab.zc.enrichmentcache.infrastructure.adapters.outbound.redis.CacheStoreRedisAdapter;

@Configuration
@EnableRedisRepositories(basePackageClasses = { CacheStoreRedisAdapter.class })
public class AppConfig {
    @Bean
    JsonMapper objectMapper() {
        var jsonMapper = JsonMapper.builder().build();

        // let Jackson discover every Jackson modules in the classpath
        // we expect to have the org.zalando.Problem module
        jsonMapper.findAndRegisterModules();

        // configure Jackson for storing Dates in a human readable format
        jsonMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX"));

        return jsonMapper;
    }

    @Bean
    ExternalDataProducer externalDataProducer() {
        return new ExternalDataProducerImpl();
    }
}
