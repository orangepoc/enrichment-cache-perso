package io.gitlab.zc.enrichmentcache.core.ports.spi;

import java.util.Optional;

import io.gitlab.zc.enrichmentcache.core.domain.FinExternalData;

public interface CacheStore {
    void store(String key, FinExternalData data);
    Optional<FinExternalData> retrieve(String key);
}
