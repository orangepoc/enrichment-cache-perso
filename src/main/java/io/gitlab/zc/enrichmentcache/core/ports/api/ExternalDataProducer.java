package io.gitlab.zc.enrichmentcache.core.ports.api;

import io.gitlab.zc.enrichmentcache.core.domain.FinExternalData;

public interface ExternalDataProducer {
    FinExternalData produceExternalData(String key);
}
