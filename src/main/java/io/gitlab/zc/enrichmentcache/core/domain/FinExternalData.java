package io.gitlab.zc.enrichmentcache.core.domain;

import java.time.LocalDateTime;

public record FinExternalData(String status, String comInt, LocalDateTime dateCi, Boolean otConforme) {
}
