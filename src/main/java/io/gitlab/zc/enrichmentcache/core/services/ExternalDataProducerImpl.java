package io.gitlab.zc.enrichmentcache.core.services;

import java.time.LocalDateTime;

import io.gitlab.zc.enrichmentcache.core.domain.FinExternalData;
import io.gitlab.zc.enrichmentcache.core.ports.api.ExternalDataProducer;

public class ExternalDataProducerImpl implements ExternalDataProducer {
    @Override
    public FinExternalData produceExternalData(String key) {
        return new FinExternalData("OK", "Hello %s from Redis".formatted(key), LocalDateTime.now(), true);
    }
}
