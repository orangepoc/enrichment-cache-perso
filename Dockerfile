FROM openjdk:17-slim

WORKDIR /app

COPY target/enrichment-cache-perso.jar application.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "application.jar"]
