# enrichment-cache-mse

A microservice to cache intervention "external data" upstream of SANKO. 
The cached data can then be consumed by another service downstream to 
SANKO.


# Redis
The microservice store data in a Redis instance configured via the `spring.redis` properties.
It stores (key,Value) tuples with the key prefixed with `intervention:cache:`. Tuples are kept in the 
store for 15 minutes (configurable) when not consumed and 15 seconds (configurable) after having being consumed .

# Observability

This microservice is amply observable through the `/actuator` endpoint.

## `health` endpoint

Microservice's health is observable via the `/actuator/health` endpoint.

The `/actuator/health` returned data is augmented with information coming from a custom FTP health indicator.


## `info` endpoint

The `/actuator/info` returned data is augmented with the following information:

| group            | what                                           |
|------------------|------------------------------------------------|
| `build`          | Information aggregated at maven build time     |
| `git`            | Information aggregated from the git repository |
| `feature-flags`  | The microservice's feature-flag list           |
| `api-dependency` | The microservice's api-dependencies list       |
| `active-profile` | The spring active profile                      |


## `featureflags` endpoint

Allow to list and set the microservice's feature-flags

### Displaying microservice's feature flags

    curl localhost:8080/actuator/featureflags

### Setting a microservice's feature flag

    curl -H 'Content-Type: application/json' -d '{"value": "true|false"}' localhost:8080/actuator/featureflags/<feature-flag-name>


## `version` endpoint

Display the microservice's name and version along with name and version of the provided and consumed APIs. 
Those information are also available in the `info` endpoint in the `api-dependency` group.


# Monitoring

Monitoring consist of the three pillars logging, metrics and [tracing](https://micrometer.io/docs/tracing#_glossary).


## `loggers` endpoint

Microservice's logger can be consulted and changed with the `/actuator/loggers/<logger-name>` endpoint.

### changing a logger level

    curl -H 'Content-Type: application/json' -d '{"configuredLevel": "<LEVEL>"}' http://localhost:8080/actuator/loggers/<logger-name>


## `metrics` endpoint

The microservice publish metrics that can be observed via this endpoint.


## `prometheus` endpoint

Exposes the same metrics in a format that can be scraped by a Prometheus server.


## Tracing

Not Yet Available


# Rest endpoint errors response

Error responses should be based on the "_[RFC 7807][1] Problem Details for HTTP APIs_" specification 
which is provided by https://github.com/zalando/problem as demonstrated in the `/spring/p3` endpoint.


> _RFC 7807 Problem Details for HTTP APIs_ is natively supported in `spring-boot-3` as
> documented in the [Spring Web MVC, §1.8 > Error Responses documentation][2] but not in `spring-boot-2`

[1]: https://www.rfc-editor.org/rfc/rfc7807.html
[2]: https://docs.spring.io/spring-framework/docs/current/reference/html/web.html#mvc-ann-rest-exceptions


# Spring-boot-3

Being a new project, the temptation was great to start with `spring-boot-3`, but the migration from the 
`javax.*` package to the `jakarta.*` package is a serious obstacle to the adoption of `spring-boot-3` right now. 

As stated in the [spring-boot-3 migration guide][3], it is wise to wait until all used libraries 
,including zaidan libs, have all been migrated to the new `jakarta.*` package.

When the time will come, it will be wise to use the [spring-boot-migrator][4] which will prepare a report of
all the changes to perform the migration - the changes can then be applied directly from the report via 
[OpenMigrate][5] recipes. Each recipes create a new commit in the current branch, the commit can then be
reviewed and accepted or reset before applying the next recipe.

[3]: https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-3.0-Migration-Guide
[4]: https://github.com/spring-projects-experimental/spring-boot-migrator
[5]: https://docs.openrewrite.org/reference/recipes/java/migrate/jakarta/javaxmigrationtojakarta
